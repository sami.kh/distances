import time
import numpy as np
import pandas as pd
import Utility as Util


if __name__ == '__main__':

    np.random.seed(42)
    x = np.random.uniform(0, 5, 1000)
    y = np.random.uniform(0, 5, 1000)

    dtw_time = time.time()
    dist_dtw = Util.get_distance(x, y, measure='dtw')
    dtw_time = str(round(time.time() - dtw_time, 2)) + 's'

    dtw_0_time = time.time()
    dist_dtw_0 = Util.get_distance(x, y, measure='dtw', window=0)
    dtw_0_time = str(round(time.time() - dtw_0_time, 2)) + 's'

    l1_time = time.time()
    dist_l1 = Util.get_distance(x, y, measure='l1')
    l1_time = str(round(time.time() - l1_time, 2)) + 's'

    l2_time = time.time()
    dist_l2 = Util.get_distance(x, y, measure='l2')
    l2_time = str(round(time.time() - l2_time, 3)) + 's'

    cort_dtw_time = time.time()
    dist_cort_dtw = Util.get_distance(x, y, measure='cort_dtw')
    cort_dtw_time = str(round(time.time() - cort_dtw_time, 2)) + 's'

    cort_time = time.time()
    dist_cort = Util.get_distance(x, y, measure='cort')
    cort_time = str(round(time.time() - cort_time, 2)) + 's'

    twed_time = time.time()
    dist_twed = Util.get_distance(x, y, measure='twed')
    twed_time = str(round(time.time() - twed_time, 2)) + 's'

    print('dtw', round(dist_dtw, 2), dtw_time)
    assert round(dist_dtw, 2) == 771.2

    print('dtw_0', round(dist_dtw_0, 2), dtw_0_time)
    assert round(dist_dtw_0, 2) == 1665.45

    print('l1', round(dist_l1, 2), l1_time)
    assert round(dist_l1, 2) == 1665.45

    print('l2', round(dist_l2, 2), l2_time)
    assert round(dist_l2, 2) == 64.39

    print('cort_dtw', round(dist_cort_dtw, 2), cort_dtw_time)
    assert round(dist_cort_dtw, 2) == 220.56

    print('cort', round(dist_cort, 2), cort_time)
    assert round(dist_cort, 2) == 0.19

    print('twed', round(dist_twed, 2), twed_time)
    assert round(dist_twed, 2) == 4377.33

    test_mat = pd.read_csv('toy_example.csv', header=None).values
    mat = Util.get_distance_matrix(test_mat)
    print('row test', round(mat[0, 1], 2))
    assert round(mat[0][1], 2) == 2.6

    mat = Util.get_distance_matrix(test_mat, axis=1)
    print('column test', round(mat[0, 1], 2))
    assert round(mat[0][1], 2) == 7.89

    print()
    print('Success!')
