
CC=gcc
CFLAGS=-Wall

all: cdtw_main

libdtw.so: cdtw.o
	$(CC) $(CFLAGS) -shared -o $@ $<

%.o: %.c %.h
	$(CC) $(CFLAGS) -c -fpic -o $@ -c $< 

cdtw_main: cdtw.c main_cdtw.c
	$(CC) $(CFLAGS) main_cdtw.c -o $@ -lm
