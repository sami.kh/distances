import itertools
import numpy as np
import distances as dist
import DistanceObject as Do


def get_distance_matrix(x=None, measure='dtw', window=None, axis=0):
    """
    Compute the distance matrix for a list of sequences using one of the following measures:
    [L1, L2, DTW, Cort, Cort_DTW, TWED]

    :param x: np matrix (array of sequences)
    :param measure: the measure to calculate
    :param window: window look-ahead for DTW, CorT and CorT_DTW
    :param axis: 0 for calculating distances between rows, 1 between columns
    :return: the distance matrix
    """

    if axis == 1:
        x = x.T

    arr_len = len(x)
    dist_matrix = np.zeros((arr_len, arr_len), dtype=np.float64)

    for i, j in itertools.combinations(range(arr_len), 2):
        temp_x = np.array(x[i], dtype=np.float64)
        temp_y = np.array(x[j], dtype=np.float64)
        dist_matrix[i, j] = dist_matrix[j, i] = get_distance(x=temp_x, y=temp_y, measure=measure, window=window)

    return dist_matrix


def get_distance(x=None, y=None, w=None, measure=None, window=None,
                 get_path=False, path=None, switch_path=False):

    """
    Compute the distance between two numpy array using one of the following measures:
    [L1, L2, DTW, Cort, Cort_DTW, TWED]

    :param x: an np array of float values
    :param y: an np array of float values
    :param w: an np array of float values (weights)
    :param measure: the measure to calculate
    :param window: window look-ahead for DTW, CorT and CorT_DTW
    :param get_path: Return the path or not (DTW, CorT, CorT_DTW)
    :param path: use an already calculated path in the distance computation
    :param switch_path: switch each pair of the path (i, j) --> (j, i)
    :return: the distance value
    """

    if measure in ['l1', 'l2']:
        return known_measures[measure](x, y, w, None)

    return known_measures[measure](x, y, w, None, None, window)


def norm_l1(x, y, w=None, get_path=False):

    """
    :param x: an np array of float values (first sequence)
    :param y: an np array of float values (second sequence)
    :param w: an np array of float values (weights)
    :param get_path: Return the array of temporal distances between x and y
    :return: the distance value
    """

    dist_obj = Do.DistanceObject()

    d = x - y
    if get_path:
        dist_obj.path = np.fabs(d)

    if w is None:
        dist_obj.additive_dist = np.linalg.norm(d, 1)
    else:
        dist_obj.additive_dist = np.linalg.norm(w * d, 1)

    return dist_obj.additive_dist


def norm_l2(x, y, w=None, get_path=False):

    """
    :param x: an np array of float values (first sequence)
    :param y: an np array of float values (second sequence)
    :param w: an np array of float values (weights)
    :param get_path: Return the array of temporal distances between x and y
    :return: the distance value
    """

    dist_obj = Do.DistanceObject()

    if get_path:
        dist_obj.path = np.fabs(x - y)

    if w is None:
        dist_obj.additive_dist = np.linalg.norm(x - y, 2)
    else:
        d = x - y
        dist_obj.additive_dist = np.sqrt((w * d * d).sum())
    return dist_obj.additive_dist


def dtw(x, y, w=None, get_path=False, path=None, window=None):

    """
    :param x: an np array of float values (first sequence)
    :param y: an np array of float values (second sequence)
    :param w: an np array of float values (weights)
    :param get_path: Return the path or not (DTW, CORT, CORT_DTW)
    :param path: Given path to be used in the distance computation
    :param window: window look-ahead for DTW, CorT and CorT_DTW
    :return: the distance value
    """

    return dist.dtw(x, y, weight=w, get_path=get_path, path=path, window=window)


def twed(x, y, w=None, get_path=False, path=None, window=None):

    """
    :param x: an np array of float values (first sequence)
    :param y: an np array of float values (second sequence)
    :param w: None
    :param get_path: None
    :param path: None
    :param window: None
    :return: the distance value
    """

    return dist.twed(x, y)


def cort(x, y, w=None, get_path=False, path=None, window=None):

    """
    :param x: an np array of float values (first sequence)
    :param y: an np array of float values (second sequence)
    :param w: an np array of float values (weights)
    :param get_path: Return the path or not (DTW, CORT, CORT_DTW)
    :param path: Given path to be used in the distance computation
    :param window: window look-ahead for DTW, CorT and CorT_DTW
    :return: the distance value
    """

    return dist.cort(x, y, weight=w, get_path=get_path, path=path, window=window)


def cort_dtw(x, y, w=None, get_path=False, path=None, window=None):

    """
    :param x: an np array of float values (first sequence)
    :param y: an np array of float values (second sequence)
    :param w: an np array of float values (weights)
    :param get_path: Return the path or not (DTW, CORT, CORT_DTW)
    :param path: Given path to be used in the distance computation
    :param window: window look-ahead for DTW, CorT and CorT_DTW
    :return: the distance value
    """

    return dist.cort_dtw(x, y, weight=w, get_path=get_path, path=path, window=window)


known_measures = {'l1': norm_l1, 'L1': norm_l1,
                  'l2': norm_l2, 'L2': norm_l2,
                  'dtw': dtw, 'DTW': dtw,
                  'cort': cort, 'CORT': cort,
                  'cort_dtw': cort_dtw, 'CORT_DTW': cort_dtw,
                  'twed': twed, 'TWED': twed}
